class CreateCatalogs < ActiveRecord::Migration
  def change
    create_table :catalogs do |t|
      t.string :name, null: false
      t.string :code, null: false
      t.text :description
      t.text :anons
      t.decimal :price, default: 0
      t.integer :count, default: 1

      t.timestamps
    end
  end
end
