class RemoveRatioFieldFromCatalogs < ActiveRecord::Migration
  def change
    remove_column :catalogs, :ratio
  end
end
