class AddSortFieldToProducts < ActiveRecord::Migration
  def change
    add_column :products, :sort, :integer
  end
end
