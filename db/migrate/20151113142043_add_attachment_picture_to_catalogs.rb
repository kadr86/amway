class AddAttachmentPictureToCatalogs < ActiveRecord::Migration
  def self.up
    change_table :catalogs do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :catalogs, :picture
  end
end
