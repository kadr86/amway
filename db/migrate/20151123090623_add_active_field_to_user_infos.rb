class AddActiveFieldToUserInfos < ActiveRecord::Migration
  def change
    add_column :user_infos, :active, :boolean
  end
end
