class AddSubsectionIdToCatalogs < ActiveRecord::Migration
  def change
    add_reference :catalogs, :subsection, index: true
  end
end
