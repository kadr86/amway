class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :name
      t.string :sename
      t.string :phone
      t.string :email
      t.string :seriya
      t.string :nomber
      t.datetime :date
      t.string :who
      t.references :product
      t.integer :count

      t.timestamps
    end
  end
end
