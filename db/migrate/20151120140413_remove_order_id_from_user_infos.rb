class RemoveOrderIdFromUserInfos < ActiveRecord::Migration
  def change
    remove_column :user_infos, :order_id
  end
end
