class CreateUserInfos < ActiveRecord::Migration
  def change
    create_table :user_infos do |t|
      t.string :name
      t.string :sename
      t.string :father_name
      t.string :phone
      t.string :email
      t.string :session

      t.timestamps
    end
  end
end
