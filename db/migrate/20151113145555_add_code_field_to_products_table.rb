class AddCodeFieldToProductsTable < ActiveRecord::Migration
  def change
    add_column :products, :code, :string, null: false
  end
end
