class AddProductIdToBrands < ActiveRecord::Migration
  def change
    add_reference :brands, :product, index: true
  end
end
