class AddRatioToCatalogs < ActiveRecord::Migration
  def change
    add_column :catalogs, :ratio, :integer, default: 0
  end
end
