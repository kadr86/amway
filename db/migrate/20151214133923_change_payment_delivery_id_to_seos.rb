class ChangePaymentDeliveryIdToSeos < ActiveRecord::Migration
  def change
    rename_column :seos, :PaymentDelivery_id, :payment_delivery_id
  end
end
