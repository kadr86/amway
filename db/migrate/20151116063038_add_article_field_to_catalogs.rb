class AddArticleFieldToCatalogs < ActiveRecord::Migration
  def change
    add_column :catalogs, :article, :string
  end
end
