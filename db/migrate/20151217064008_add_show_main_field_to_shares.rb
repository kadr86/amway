class AddShowMainFieldToShares < ActiveRecord::Migration
  def change
    add_column :shares, :show_main, :boolean
  end
end
