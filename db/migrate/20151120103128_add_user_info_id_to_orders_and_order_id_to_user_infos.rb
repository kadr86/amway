class AddUserInfoIdToOrdersAndOrderIdToUserInfos < ActiveRecord::Migration
  def change
    add_column :orders, :user_info_id, :integer
    add_column :user_infos, :order_id, :integer
  end
end
