class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :name
      t.text :comment
      t.references :catalog
      t.references :ratio

      t.timestamps
    end
  end
end
