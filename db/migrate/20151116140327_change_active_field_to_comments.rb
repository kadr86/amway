class ChangeActiveFieldToComments < ActiveRecord::Migration
  def change
    change_column :comments, :active, :boolean, default: false
  end
end
