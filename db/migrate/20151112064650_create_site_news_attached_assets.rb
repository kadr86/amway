class CreateSiteNewsAttachedAssets < ActiveRecord::Migration
  def change
    create_table :site_news_attached_assets do |t|
      t.attachment :picture
      t.attachment :asset
      t.integer :attachable_id
      t.integer :attachable_type

      t.timestamps
    end
  end
end
