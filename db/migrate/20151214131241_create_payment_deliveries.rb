class CreatePaymentDeliveries < ActiveRecord::Migration
  def change
    create_table :payment_deliveries do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
