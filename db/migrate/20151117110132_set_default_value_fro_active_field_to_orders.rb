class SetDefaultValueFroActiveFieldToOrders < ActiveRecord::Migration
  def change
    change_column :orders, :active, :boolean, default: 0
  end
end
