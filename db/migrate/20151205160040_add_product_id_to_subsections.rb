class AddProductIdToSubsections < ActiveRecord::Migration
  def change
    add_reference :subsections, :product, index: true
  end
end
