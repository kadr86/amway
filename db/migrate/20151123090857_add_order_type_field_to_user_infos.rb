class AddOrderTypeFieldToUserInfos < ActiveRecord::Migration
  def change
    add_column :user_infos, :order_type, :integer
  end
end
