class AddActiveFieldToSharesAndSiteNews < ActiveRecord::Migration
  def change
    add_column :shares, :active, :boolean, default: true
    add_column :site_news, :active, :boolean, default: true
  end
end
