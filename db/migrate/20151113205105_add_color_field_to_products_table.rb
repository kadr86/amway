class AddColorFieldToProductsTable < ActiveRecord::Migration
  def change
    add_column :products, :color, :string, default: 'teal'
  end
end
