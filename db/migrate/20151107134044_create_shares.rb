class CreateShares < ActiveRecord::Migration
  def change
    create_table :shares do |t|
      t.string :name
      t.text :anons
      t.text :detail
      t.datetime :date_from
      t.datetime :date_to

      t.timestamps
    end
  end
end
