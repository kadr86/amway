class AddDiscountToCatalogs < ActiveRecord::Migration
  def change
    add_column :catalogs, :discount, :integer
  end
end
