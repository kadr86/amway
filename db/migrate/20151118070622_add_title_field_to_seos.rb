class AddTitleFieldToSeos < ActiveRecord::Migration
  def change
    add_column :seos, :title, :string
  end
end
