class ChangeSiteNewsAttacheAssets < ActiveRecord::Migration
  def change
    rename_column :site_news_attached_assets, :attachable_id, :site_news_attachable_id
    rename_column :site_news_attached_assets, :attachable_type, :site_news_attachable_type
  end
end
