class RenameProductIdToCatalogIdOnOrders < ActiveRecord::Migration
  def change
    rename_column :orders, :product_id, :catalog_id
  end
end
