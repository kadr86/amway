class AddAttachmentPictureToSiteNews < ActiveRecord::Migration
  def self.up
    change_table :site_news do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :site_news, :picture
  end
end
