class ChangeDateForDateToTypeOnShares < ActiveRecord::Migration
  def change
    change_column :shares, :date_from, :string
    change_column :shares, :date_to, :string
  end
end
