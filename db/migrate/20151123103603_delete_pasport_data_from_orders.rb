class DeletePasportDataFromOrders < ActiveRecord::Migration
  def change
    remove_column :orders, :seriya
    remove_column :orders, :number
    remove_column :orders, :who
    remove_column :orders, :date
    add_column :user_infos, :seriya, :string
    add_column :user_infos, :number, :string
    add_column :user_infos, :who, :string
    add_column :user_infos, :date, :date

  end
end
