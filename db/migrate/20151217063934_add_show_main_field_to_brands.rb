class AddShowMainFieldToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :show_main, :boolean
  end
end
