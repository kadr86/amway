class ChangeActiveFieldOnUserInfos < ActiveRecord::Migration
  def change
    change_column :user_infos, :active, :boolean, default: 0
  end
end
