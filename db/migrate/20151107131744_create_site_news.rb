class CreateSiteNews < ActiveRecord::Migration
  def change
    create_table :site_news do |t|
      t.string :name
      t.text :anons
      t.text :detail

      t.timestamps
    end
  end
end
