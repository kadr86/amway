class AddCommentIdToRatio < ActiveRecord::Migration
  def change
    add_column :ratios, :comment_id, :integer
  end
end
