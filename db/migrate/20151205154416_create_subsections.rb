class CreateSubsections < ActiveRecord::Migration
  def change
    create_table :subsections do |t|
      t.string :name

      t.timestamps
    end
  end
end
