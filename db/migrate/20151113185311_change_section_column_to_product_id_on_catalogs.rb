class ChangeSectionColumnToProductIdOnCatalogs < ActiveRecord::Migration
  def change
    rename_column :catalogs, :section, :product_id
  end
end
