class AddPaymentDeliveryIdToSeos < ActiveRecord::Migration
  def change
    add_reference :seos, :PaymentDelivery, index: true
  end
end
