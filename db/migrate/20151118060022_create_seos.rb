class CreateSeos < ActiveRecord::Migration
  def change
    create_table :seos do |t|
      t.references :main, index: true
      t.references :product, index: true
      t.references :catalog, index: true
      t.references :bisnes, index: true
      t.references :site_new, index: true
      t.references :share, index: true
      t.references :photogallery, index: true
      t.references :contact, index: true
      t.string :keywords
      t.text :desctiption

      t.timestamps
    end
  end
end
