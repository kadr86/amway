class AddBrandIdToCatalogs < ActiveRecord::Migration
  def change
    add_reference :catalogs, :brand, index: true
  end
end
