class AddSortFieldToCatalogs < ActiveRecord::Migration
  def change
    add_column :catalogs, :sort, :integer
  end
end
