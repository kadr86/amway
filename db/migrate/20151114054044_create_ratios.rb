class CreateRatios < ActiveRecord::Migration
  def change
    create_table :ratios do |t|
      t.attachment :catalog
      t.integer :ratio
      t.string :ip
      t.string :session

      t.timestamps
    end
  end
end
