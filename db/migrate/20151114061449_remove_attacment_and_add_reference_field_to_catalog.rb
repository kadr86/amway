class RemoveAttacmentAndAddReferenceFieldToCatalog < ActiveRecord::Migration
  def change
    remove_column :ratios, :catalog_file_name
    remove_column :ratios, :catalog_content_type
    remove_column :ratios, :catalog_file_size
    remove_column :ratios, :catalog_updated_at
    add_column :ratios, :catalog_id, :integer
  end
end
