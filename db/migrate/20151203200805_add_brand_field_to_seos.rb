class AddBrandFieldToSeos < ActiveRecord::Migration
  def change
    add_reference :seos, :brand, index: true
  end
end
