# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151217071019) do

  create_table "bisnes", force: true do |t|
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title"
    t.integer  "sort"
  end

  create_table "brands", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.integer  "product_id"
    t.boolean  "show_main"
  end

  add_index "brands", ["product_id"], name: "index_brands_on_product_id", using: :btree

  create_table "catalogs", force: true do |t|
    t.string   "name",                                                      null: false
    t.string   "code",                                                      null: false
    t.text     "description"
    t.text     "anons"
    t.decimal  "price",                precision: 10, scale: 0, default: 0
    t.integer  "count",                                         default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.integer  "product_id"
    t.string   "article"
    t.integer  "brand_id"
    t.integer  "subsection_id"
    t.integer  "discount"
    t.integer  "sort"
  end

  add_index "catalogs", ["brand_id"], name: "index_catalogs_on_brand_id", using: :btree
  add_index "catalogs", ["subsection_id"], name: "index_catalogs_on_subsection_id", using: :btree

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "comments", force: true do |t|
    t.string   "name"
    t.text     "comment"
    t.integer  "catalog_id"
    t.integer  "ratio_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active",     default: false
  end

  create_table "contacts", force: true do |t|
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mains", force: true do |t|
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orders", force: true do |t|
    t.string   "name"
    t.string   "sename"
    t.string   "phone"
    t.string   "email"
    t.integer  "catalog_id"
    t.integer  "count"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "order_type"
    t.boolean  "active",       default: false
    t.string   "session"
    t.integer  "user_info_id"
  end

  create_table "payment_deliveries", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "photogalleries", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", force: true do |t|
    t.string   "name"
    t.text     "anons"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.string   "code",                                  null: false
    t.string   "color",                default: "teal"
    t.integer  "sort"
  end

  create_table "ratios", force: true do |t|
    t.integer  "ratio"
    t.string   "ip"
    t.string   "session"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "catalog_id"
    t.integer  "comment_id"
  end

  create_table "seos", force: true do |t|
    t.integer  "main_id"
    t.integer  "product_id"
    t.integer  "catalog_id"
    t.integer  "bisnes_id"
    t.integer  "site_new_id"
    t.integer  "share_id"
    t.integer  "photogallery_id"
    t.integer  "contact_id"
    t.string   "keywords"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title"
    t.integer  "brand_id"
    t.integer  "payment_delivery_id"
  end

  add_index "seos", ["bisnes_id"], name: "index_seos_on_bisnes_id", using: :btree
  add_index "seos", ["brand_id"], name: "index_seos_on_brand_id", using: :btree
  add_index "seos", ["catalog_id"], name: "index_seos_on_catalog_id", using: :btree
  add_index "seos", ["contact_id"], name: "index_seos_on_contact_id", using: :btree
  add_index "seos", ["main_id"], name: "index_seos_on_main_id", using: :btree
  add_index "seos", ["payment_delivery_id"], name: "index_seos_on_payment_delivery_id", using: :btree
  add_index "seos", ["photogallery_id"], name: "index_seos_on_photogallery_id", using: :btree
  add_index "seos", ["product_id"], name: "index_seos_on_product_id", using: :btree
  add_index "seos", ["share_id"], name: "index_seos_on_share_id", using: :btree
  add_index "seos", ["site_new_id"], name: "index_seos_on_site_new_id", using: :btree

  create_table "shares", force: true do |t|
    t.string   "name"
    t.text     "anons"
    t.text     "detail"
    t.string   "date_from"
    t.string   "date_to"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.boolean  "show_main"
    t.boolean  "active",               default: true
  end

  create_table "site_news", force: true do |t|
    t.string   "name"
    t.text     "anons"
    t.text     "detail"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.boolean  "active",               default: true
  end

  create_table "site_news_attached_assets", force: true do |t|
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.string   "asset_file_name"
    t.string   "asset_content_type"
    t.integer  "asset_file_size"
    t.datetime "asset_updated_at"
    t.integer  "site_news_attachable_id"
    t.integer  "site_news_attachable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subsections", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_id"
  end

  add_index "subsections", ["product_id"], name: "index_subsections_on_product_id", using: :btree

  create_table "user_infos", force: true do |t|
    t.string   "name"
    t.string   "sename"
    t.string   "father_name"
    t.string   "phone"
    t.string   "email"
    t.string   "session"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active",      default: false
    t.integer  "order_type"
    t.string   "seriya"
    t.string   "number"
    t.string   "who"
    t.date     "date"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
