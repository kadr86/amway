class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include SimpleCaptcha::ControllerHelpers
  protect_from_forgery with: :exception
  add_breadcrumb 'Главная', :root_path
  layout 'admin', except: [:index, :show, :section]

  def after_sign_in_path_for(resource_or_scope)
    admin_path
  end

  def after_sign_out_path_for(resource_or_scope)
    request.referrer
  end


  def admin_cabinet
    render '/layouts/admin/index'
  end

  def sitemap
    headers['Content-Type'] = 'application/xml'
    sitemap = '<?xml version="1.0" encoding="UTF-8"?>'
    sitemap = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'
    sitemap += "<url>
                  <loc>#{root_url()}</loc>                  
                </url>"
    sitemap += "<url><loc>#{catalogs_url()}</loc></url>"
    Product.all.each do |product|
      sitemap += "<url><loc>#{section_catalog_url(product.code)}</loc></url>"
      product.subsections.each do |subsection|
        sitemap += "<url><loc>#{catalog_subsection_url(product.code, subsection.id)}</loc></url>"
        subsection.catalogs.each do |catalog|
         sitemap += "<url><loc>#{catalog_url(catalog)}</loc></url>"
        end
      end
      product.brands.each do |brand|
        sitemap += "<url><loc>#{brand_catalogs_url(product.code, brand.id)}</loc></url>"
      end
    end
    sitemap += "<url><loc>#{bisneses_url()}</loc></url>"
    sitemap += "<url><loc>#{site_news_index_url()}</loc></url>"
    SiteNew.all.each do |site_new|
      sitemap += "<url><loc>#{site_news_url(site_new)}</loc></url>"
    end
    sitemap += "<url><loc>#{shares_url()}</loc></url>"
    Share.all.each do |share|
      sitemap += "<url><loc>#{share_url(share)}</loc></url>"
    end
    sitemap += "<url><loc>#{photogallereis_url()}</loc></url>"
    sitemap += "<url><loc>#{contacts_url()}</loc></url>"
    sitemap += '</urlset>'


    render xml: sitemap
  end
end
