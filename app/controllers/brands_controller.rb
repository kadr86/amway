class BrandsController < ApplicationController
  before_filter :authenticate_user!, except: [:index, :show]
  before_filter only: [:edit, :update, :show, :destroy] do
    @brands = Brand.find params[:id]
  end
  add_breadcrumb 'Бренды', :brands_path

  def index
    @brands = Brand.page(params[:page]).per(10)
  end

  def admin_index
    @brands = Brand.all
  end

  def new
    @brands = Brand.new
    @brands.build_seo
  end

  def create
    @brands = Brand.new(request_params)
    if @brands.save()
      redirect_to admin_brands_path
    else
      render 'new'
    end
  end

  def edit
    @brands.build_seo if @brands.seo.nil?
  end

  def update
    if @brands.update(request_params)
      redirect_to admin_brands_path
    else
      render 'edit'
    end
  end

  def show
    add_breadcrumb @brands.name
  end

  def destroy
    if @brands.destroy
      redirect_to admin_brands_path
    else
      render 'admin_index'
    end
  end

  def request_params
    params.require(:brand).permit(:name,:description, :picture, :product_id, :show_main,  seo_attributes:[:keywords, :description, :title])
  end
end
