class MainsController < ApplicationController
  before_filter :authenticate_user!, except: [:index]
  before_filter only: [:index, :admin_index] do
    @main = Main.first
  end

  def index
    render layout: 'main'
  end

  def admin_index
  end

  def new
    @main = Main.new
    @main.build_seo
  end

  def create
    @main = Main.new(post_params)
    if @main.save()
      redirect_to admin_mains_path
    else
      render :new
    end
  end

  def edit
    @main = Main.find params[:id]
    @main.build_seo if @main.seo.nil?
  end

  def update
    @main = Main.find params[:id]
    if @main.update(post_params)
      redirect_to admin_mains_path
    else
      render :edit
    end
  end

  def post_params
    params.require(:main).permit(:text, seo_attributes:[:keywords, :description, :title])
  end
end
