class BisnesesController < ApplicationController
  before_filter :authenticate_user!, except: [:index, :show]
  before_filter only: [:admin_index] do
    @bisnes = Bisnes.all
  end
  before_filter only: [:edit, :update, :show] do
    @bisnes = Bisnes.find params[:id]
  end

  before_filter only: [:index, :show] do
    @menu = Bisnes.all
  end



  def index
    @bisnes = Bisnes.first
    add_breadcrumb @bisnes.title
  end

  def admin_index
  end

  def new
    @bisnes = Bisnes.new
    @bisnes.build_seo
  end

  def create
    @bisnes = Bisnes.new(post_params)
    if @bisnes.save()
      redirect_to admin_bisneses_path
    else
      render :new
    end
  end

  def edit
    @bisnes.build_seo if @bisnes.seo.nil?
  end

  def update
    if @bisnes.update(post_params)
      redirect_to admin_bisneses_path
    else
      render :edit
    end
  end

  def show
    add_breadcrumb @bisnes.title
  end

  def post_params
    params.require(:bisnes).permit(:text, :title, :sort, seo_attributes:[:keywords, :description, :title])
  end
end
