class ContactsController < ApplicationController
  before_filter :authenticate_user!, except: [:index]
  before_filter only: [:index, :admin_index] do
    @contact = Contact.first
  end
  before_filter only: [:edit, :update] do
    @contact = Contact.find params[:id]
  end
  add_breadcrumb 'Контакты', :contacts_path

  def index
  end

  def admin_index
  end

  def new
    @contact = Contact.new
    @contact.build_seo
  end

  def create
    @contact = Contact.new(post_params)
    if @contact.save()
      redirect_to admin_contacts_path
    else
      render :new
    end
  end

  def edit
    @contact.build_seo @contact.seo.nil?
  end

  def update
    if @contact.update(post_params)
      redirect_to admin_contacts_path
    else
      render :edit
    end
  end

  def post_params
    params.require(:contact).permit(:text, seo_attributes:[:keywords, :description, :title])
  end
end
