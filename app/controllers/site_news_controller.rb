class SiteNewsController < ApplicationController
  before_filter :authenticate_user!, except: [:index, :show]
  before_filter only: [:edit, :update, :show, :destroy] do
    @site_news = SiteNew.unscoped.find params[:id]
  end
  add_breadcrumb 'Новости', :site_news_index_path

  def index
    @site_news = SiteNew.page(params[:page]).per(5)
  end

  def admin_index
    @site_news = SiteNew.unscoped.order(active: :desc, created_at: :desc).page(params[:page]).per(20)
  end

  def new
    @site_news = SiteNew.new
    @site_news.build_seo
  end

  def create
    @site_news = SiteNew.new(request_params)
    if @site_news.save()
      redirect_to admin_site_news_index_path
    else
      render 'new'
    end
  end

  def edit
    @site_news.build_seo if @site_news.seo.nil?
  end

  def update
    if @site_news.update(request_params)
      redirect_to admin_site_news_index_path
    else
      render 'edit'
    end
  end

  def show
    add_breadcrumb @site_news.name
  end

  def destroy
    if @site_news.destroy
      redirect_to admin_site_news_index_path
    else
      render 'index'
    end
  end

  def request_params
    params.require(:site_new).permit(:name,:anons, :detail, :picture, :active, seo_attributes:[:keywords, :description, :title])
  end
end
