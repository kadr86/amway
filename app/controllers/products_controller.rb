class ProductsController < ApplicationController
  before_filter :authenticate_user!
  before_filter only: [:edit, :update, :destroy] do
    @products = Product.find params[:id]
  end
  def new
    @products = Product.new
    @products.build_seo
  end

  def create
    @products = Product.new(request_params)
    if @products.save()
      redirect_to admin_catalogs_path
    else
      render 'new'
    end
  end

  def edit
    @products.build_seo if @products.seo.nil?
  end

  def update
    if @products.update(request_params)
      redirect_to admin_catalogs_path
    else
      render 'edit'
    end
  end

  def destroy
    if @products.destroy
      redirect_to admin_catalogs_path
    else
      render 'index'
    end
  end

  def request_params
    params.require(:product).permit(:name, :color, :picture, :anons, :sort, seo_attributes:[:keywords, :description, :title])
  end
end
