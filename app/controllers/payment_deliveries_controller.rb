class PaymentDeliveriesController < ApplicationController
  before_filter :authenticate_user!, except: [:index, :show]
  before_filter only: [:edit, :update, :show, :destroy] do
    @payment_delivery = PaymentDelivery.find params[:id]
  end
  before_action only: [:index, :show] do
    @menu = PaymentDelivery.all
  end

  def index
    @payment_delivery = PaymentDelivery.first
    add_breadcrumb @payment_delivery.name
  end

  def admin_index
    @payment_deliveries = PaymentDelivery.all
  end

  def new
    @payment_delivery = PaymentDelivery.new
    @payment_delivery.build_seo
  end

  def create
    @payment_delivery = PaymentDelivery.new(request_params)
    if @payment_delivery.save()
      redirect_to admin_payment_deliveries_path
    else
      render 'new'
    end
  end

  def edit
    @payment_delivery.build_seo if @payment_delivery.seo.nil?
  end

  def update
    if @payment_delivery.update(request_params)
      redirect_to admin_payment_deliveries_path
    else
      render 'edit'
    end
  end

  def show
    add_breadcrumb @payment_delivery.name
  end

  def request_params
    params.require(:payment_delivery).permit(:name, :description, seo_attributes:[:keywords, :description, :title])
  end
end
