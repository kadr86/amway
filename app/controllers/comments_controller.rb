class CommentsController < ApplicationController
  before_filter :authenticate_user!
  before_filter only: [:edit, :update, :show, :destroy] do
    @comments = Comment.unscoped.find( params[:id])
  end
  def admin_index
    @comments = Comment.unscoped.order(:created_at).page(params[:page]).per(20)
  end

  def edit
  end

  def update
    if @comments.update(params_request)
      redirect_to admin_comments_path
    else
      render 'edit'
    end
  end

  def show
  end

  def destroy
    if @comments.destroy()
      redirect_to admin_comments_path
    else
      redirect_to admin_comments_path
    end
  end

    def params_request
      params.require(:comment).permit(:name, :comment, :active)
    end
end
