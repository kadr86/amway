class CatalogsController < ApplicationController
  before_filter :authenticate_user!, except: [:index, :show, :section, :add_comment, :discount, :brand, :subsection]
  before_filter only: [:edit, :update, :show, :destroy] do
    @catalogs = Catalog.find params[:id]
  end
  add_breadcrumb 'Продукция', :catalogs_path
  def index
    @products = Product.all
    render layout: 'catalog_section'
  end

  def section
   @product = Product.find_by code: params[:code]
   add_breadcrumb @product.name
   render layout: 'catalog_section'
  end

  def subsection
   @product = Product.find_by code: params[:code]
   @subsection = @product.subsections.find params[:sub_id]
   add_breadcrumb @subsection.name
   render layout: 'catalog_section'
  end

  def brand
    @product = Product.find_by code: params[:code]
    @brand = Brand.find params[:brand_id]
    add_breadcrumb @brand.name
    render layout: 'catalog_section'
  end

  def discount
    @catalogs = Catalog.where.not discount: :null
    add_breadcrumb 'Продукция со скидками'
    render layout: 'catalog_section'
  end

  def admin_index
    @products = Product.all
  end

  def new
    @catalogs = Catalog.new
    @catalogs.build_seo
  end

  def create
    @catalogs = Catalog.new(request_params)
    if @catalogs.save()
      redirect_to admin_catalogs_path
    else
      render 'new'
    end
  end

  def edit
    @catalogs.build_seo if @catalogs.seo.nil?
  end

  def update
    if @catalogs.update(request_params)
      redirect_to admin_catalogs_path
    else
      render 'edit'
    end
  end

  def show
    add_breadcrumb @catalogs.product.name, section_catalog_path(@catalogs.product.code)
    add_breadcrumb @catalogs.name
    @comments = Comment.new
    @comments.build_ratio
  end

  def destroy
    if @catalogs.destroy
      redirect_to admin_catalogs_path
    else
      render 'index'
    end
  end

  def add_comment
    # Ratio.create!(catalog_id: params[:id], ratio: params[:ratio], ip: params[:ip], session: params[:session])
    @comment = Comment.new(comment_params)
    if  @comment.save()
      ActionMailer::Base.default_url_options[:host] = request.host_with_port
      AddComment.user_add_comment(@comment.id, @comment.name, @comment.comment, @comment.catalog_id).deliver!
      redirect_to catalog_path(@comment.catalog.id), notice: 'Коментарий был добавлен, после модерации будет доступен на сайте.'
    else
      render 'show'
    end
  end



  def request_params
    params.require(:catalog).permit(:name, :anons, :description, :price, :count, :picture, :product_id, :article, :brand_id, :subsection_id, :discount, :sort, seo_attributes:[:keywords, :description, :title])
  end

  def comment_params
    params.require(:comment).permit(:active, :catalog_id, :name, :comment, ratio_attributes:[:ip, :ratio, :session, :catalog_id], seo_attributes:[:keywords, :description, :title])
  end
end
