class PhotogallereisController < ApplicationController
  before_filter :authenticate_user!, except: [:index, :show]
  add_breadcrumb 'Фотогалерея', :photogallereis_path
  def index
  end

  def new
  end

  def edit
  end

  def show
    add_breadcrumb 'Amagrams'
  end
end
