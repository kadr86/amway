class OrdersController < ApplicationController
  before_filter :authenticate_user!, except: [:create, :edit, :update, :index, :success, :accept_order]
  add_breadcrumb 'Корзина', :edit_order_path

  def index
    @user_info = UserInfo.find_by session: params[:session]
    @orders = Order.where(order_type: 2, session: @user_info.session, active: false).order(created_at: :asc)
    render layout: 'application'
  end

  # def admin_index
  #   @users = UserInfo.order(created_at: :desc).page(params[:page]).per(20)
  # end

  def new
  end

  def accept_order
    update = []
    ids = params[:orders][:id]
    ids.each_index do |i|
      update << { catalog_id: params[:orders][:catalog_id][i.to_i], user_info_id: params[:orders][:user_info_id][i.to_i], count: params[:orders][:count][i.to_i], active: 1 }
    end

    if @order = Order.update(ids, update)

      redirect_to success_orders_path(id: @order.first.user_info_id)
    else
      render 'index'
    end
  end

  def success
    @user_info = UserInfo.find params[:id]
    ActionMailer::Base.default_url_options[:host] = request.host_with_port
    AddOrder.user_add_order(@user_info.id, @user_info.name, @user_info.sename, @user_info.father_name, @user_info.phone, @user_info.email, @user_info.created_at, 2).deliver!
    AddOrder.success(@user_info.email, @user_info.id).deliver!
    render layout: 'application'
  end

  def create
    Order.create!(request_params)
    render nothing:  true
  end

  def edit

  end

  def update
    Order.find(params[:id]).update(request_params)
    redirect_to admin_orders_path()
  end

  def show
    @order = Order.find params[:id]
    render layout: 'admin'
  end

  def destroy
    @order = Order.find(params[:id])
      if @order.destroy()
        redirect_to orders_path(session: @order.session), notice: "Заказ: #{@order.catalog.name} (#{@order.catalog.article}), удалён."
      else
        render 'index'
    end
  end

  def request_params
    params.require(:order).permit(:name, :sename, :phone, :email, :seriya, :number, :date, :who, :catalog_id, :count, :order_type, :active, :session)
  end

  def request_accept_params
    params.require(:orders).permit(:catalog_id, :count, :user_info_id)
  end
end
