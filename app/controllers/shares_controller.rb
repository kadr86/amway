class SharesController < ApplicationController
  before_filter :authenticate_user!, except: [:index, :show]
  before_filter only: [:edit, :update, :show, :destroy] do
    @shares = Share.unscoped.find params[:id]
  end
  add_breadcrumb 'Акции', :shares_path

  def index
    @shares = Share.page(params[:page]).per(5)
  end

  def admin_index
    @shares = Share.unscoped.order(active: :desc, created_at: :desc).page(params[:page]).per(20)
  end

  def new
    @shares = Share.new
    @shares.build_seo
  end

  def create
    @shares = Share.new(request_params)
    if @shares.save()
      redirect_to admin_shares_path
    else
      render 'new'
    end
  end

  def edit
    @shares.build_seo if @shares.seo.nil?
  end

  def update
    if @shares.update(request_params)
      redirect_to admin_shares_path
    else
      render 'edit'
    end
  end

  def show
    add_breadcrumb @shares.name
  end

  def destroy
    if @shares.destroy
      redirect_to admin_shares_path
    else
      render 'index'
    end
  end

  def request_params
    params.require(:share).permit(:name, :anons, :detail, :picture, :date_from, :date_to, :show_main, :active, seo_attributes:[:keywords, :description, :title])
  end
end
