class UserInfosController < ApplicationController
  before_filter :authenticate_user!, only: [:admin_index, :update]

  def index
  end

  def admin_index
    @users = UserInfo.order(created_at: :desc).page(params[:page]).per(20)
  end

  def new
    @user_info = UserInfo.new
    render layout: 'application'
  end

  def create
    @user_info = UserInfo.new(request_params)
    if @user_info.save()
      redirect_to orders_path(session: @user_info.session)
    else
      render 'new', layout: 'application'
    end
  end

  def add_request

    @user_info = UserInfo.new(request_params)

    status = simple_captcha_valid?

    if status && @user_info.save()
      ActionMailer::Base.default_url_options[:host] = request.host_with_port
      AddOrder.user_add_order(@user_info.id, @user_info.name, @user_info.sename, @user_info.father_name, @user_info.phone, @user_info.email, @user_info.created_at, 1).deliver!
    end
    render text: status
  end

  def edit
    @user_info = UserInfo.find params[:id]
    render layout: 'application'
  end

  def update_from_site
    @user_info = UserInfo.find(params[:id])
    if @user_info.update(request_params)
      redirect_to orders_path(session: @user_info.session)
    else
      render 'edit'
    end
  end

  def update
    UserInfo.find(params[:id]).update(request_params)
    redirect_to admin_user_infos_path()
  end

  def show
    @user = UserInfo.find params[:id]
    render layout: 'admin'
  end

  def request_params
    params.require(:user_info).permit(:name, :sename, :father_name, :phone, :email, :session, :order_id, :order_type, :active)
  end
end
