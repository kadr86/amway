class SubsectionsController < ApplicationController
  before_filter :authenticate_user!
  before_filter only: [:edit, :update, :destroy] do
    @subsection = Subsection.find params[:id]
  end


  def new
    @subsection = Subsection.new
  end

  def create
    @subsection = Subsection.new(request_params)
    if @subsection.save()
      redirect_to admin_catalogs_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @subsection.update(request_params)
      redirect_to admin_catalogs_path
    else
      render 'edit'
    end
  end

  def destroy
    if @subsection.destroy
      redirect_to admin_catalogs_path
    else
      render 'index'
    end
  end

  def request_params
    params.require(:subsection).permit(:name, :product_id)
  end
end
