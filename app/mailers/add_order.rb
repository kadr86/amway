class AddOrder < ActionMailer::Base
  default from: 'ulamway73@gmail.com'

  def user_add_order(id, name, sename, father_name, phone, email, created_at, order_type)
    @type = {1 => 'Стать партнером', 2 => 'Заказать' }
    @type = @type[order_type]
    @id = id
    @name = name
    @sename = sename
    @father_name = father_name
    @phone = phone
    @email = email
    @date = created_at.strftime('%d.%m.%Y %H:%M')
    @host = ActionMailer::Base.default_url_options[:host]
    to = 'ulamway73@gmail.com,kadr1986@gmail.com,markate1986@gmail.com'
    mail(to: to,  subject: 'Добавлен новый заказ на сайт')

  end

  def success(email, ordernumber)
    @ordernumber = ordernumber
    mail(to: email, subject: 'Заказ принят')
  end
end
