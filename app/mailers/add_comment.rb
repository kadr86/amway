class AddComment < ActionMailer::Base
  default from: 'amway@site.ru'

  def user_add_comment(id, name, message, catalog_id)
    @id = id
    @name = name
    @message = message
    @product = Catalog.find(catalog_id)
    to = 'ulamway73@gmail.com'
    mail(to: to,  subject: "Добавлен новый комментарый для #{@product.name}")

  end
end
