class Seo < ActiveRecord::Base
  belongs_to :main
  belongs_to :product
  belongs_to :catalog
  belongs_to :bisnes
  belongs_to :brand
  belongs_to :site_new
  belongs_to :share
  belongs_to :photogallery
  belongs_to :contact
  belongs_to :payment_delivery
end
