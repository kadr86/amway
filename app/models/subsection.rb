class Subsection < ActiveRecord::Base
  belongs_to :product
  has_many :catalogs
end
