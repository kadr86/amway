class Main < ActiveRecord::Base
  has_one :seo, dependent: :destroy
  accepts_nested_attributes_for :seo, allow_destroy: true
end
