class Brand < ActiveRecord::Base
  has_many :catalogs
  belongs_to :product
  has_one :seo, dependent: :destroy
  accepts_nested_attributes_for :seo, allow_destroy: true
  has_attached_file :picture, styles: { mini: '100x', site: '100x', normal: '450x450#', detail: '300x350#' }
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\Z/

  scope :show_main, -> {where show_main: true}
end
