class SiteNew < ActiveRecord::Base
  has_one :seo, dependent: :destroy
  accepts_nested_attributes_for :seo, allow_destroy: true
  has_attached_file :picture, styles: { mini: '48x48#', site: '150x150#', normal: '450x450#', detail: '300x350#' }
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\Z/
  default_scope {where(active: true).order(created_at: :desc)}


end
