class Order < ActiveRecord::Base
  # validates :name, :sename, :phone, presence: true
  validates :count, numericality: {min: 1}, allow_nil: true, allow_blank: true
  belongs_to :catalog
  belongs_to :user_info
end
