class Product < ActiveRecord::Base
  has_many :catalogs, dependent: :delete_all
  has_many :brands
  has_many :subsections
  has_one :seo, dependent: :destroy
  accepts_nested_attributes_for :seo, allow_destroy: true
  has_attached_file :picture, styles: { mini: '48x48#', site: '336x249#', normal: '450x450#'}
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\Z/

  default_scope {order sort: :asc}

  before_save do
    self.code = name.to_slug_param
  end

  before_update do
    self.code = name.to_slug_param
  end
end
