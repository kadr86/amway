class Comment < ActiveRecord::Base
  belongs_to :catalog
  has_one :ratio, dependent: :destroy
  accepts_nested_attributes_for :ratio, allow_destroy: true

  default_scope {where(active: true).order(:created_at)}
end
