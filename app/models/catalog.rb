class Catalog < ActiveRecord::Base
  belongs_to :product
  belongs_to :brand
  has_many :ratios
  has_many :comments
  has_many :orders
  has_one :seo, dependent: :destroy
  accepts_nested_attributes_for :seo, allow_destroy: true

  has_attached_file :picture, styles: { mini: '48x48#', site: '290x300!', detal: '230x'}, default_url: '/images/:style/missing.jpg'
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\Z/
  validates :name, presence: true
  before_save do
    self.code = name.to_slug_param
  end

  before_update do
    self.code = name.to_slug_param
  end
end
