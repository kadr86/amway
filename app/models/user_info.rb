class UserInfo < ActiveRecord::Base
  has_many :orders, dependent: :destroy
  validates :name, :sename, :phone, presence: true
  apply_simple_captcha message: 'Код на картинке отличается от введенного', add_to_base: true
end
