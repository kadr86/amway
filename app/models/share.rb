class Share < ActiveRecord::Base
  has_one :seo, dependent: :destroy
  accepts_nested_attributes_for :seo, allow_destroy: true
  has_attached_file :picture, styles: {mini: '48x48#', site: '971x400#', admin: '450x', banner: '259x', main: '1248x' }
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\Z/
  default_scope  {where(active: true).order(created_at: :desc) }

  scope :show_main, -> {where show_main: true}

  def next
    ids_db = self.class.select(:id).order(created_at: :desc)
    ids = []
    unless ids_db.nil?
      ids_db.each {|db| ids << db.id}
    end
    ids[ids.index(id) + 1]
  end

  def prev
    ids_db = self.class.select(:id).order(created_at: :desc)
    ids = []
    unless ids_db.nil?
      ids_db.each {|db| ids << db.id}
    end
    ids[ids.index(id) - 1]
  end
end
