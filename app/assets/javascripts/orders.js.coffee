$(document).on 'change', '.item_count', ->
  $this = $(@)
  count = $this.val()
  summ_items = $('.summ')
  item = $this.attr('data-order')
  item_summ = $('.item_summ_'+item)
  price = $('.item_price_'+item).val()
  summ_base = price*count
  summ = summ_base.toLocaleString('ru-RU', { style: 'currency', currency: 'RUB'})
  summ = summ.replace(/руб./g, '')
  item_summ.find('span').html(summ)
  item_summ.find('#summ').val(summ_base)
  itog_summ = 0
  itog = $('.itog')
  summ_items.each (index, val) ->
    itog_summ += parseInt($(val).find('#summ').val())

  itog_summ = itog_summ.toLocaleString('ru-RU', { style: 'currency', currency: 'RUB'})
  itog_summ = itog_summ.replace(/руб./g, '')
  itog.html(itog_summ)