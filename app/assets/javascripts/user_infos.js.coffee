$(document).on 'ready page:load', ->
  $("#user_info_phone").mask("+7 (999) 999 99 99",{placeholder:"+7 (___) ___ __ __"})
$(document).on 'change', '.active_order', ->
  form = $(@).parent().attr('id')
  $('#'+form).submit()
$(document).on 'click', '#partner_form_button', (e) ->
  e.preventDefault();
  $.ajax
    type: "post"
    url: "/user_infos/add_request"
    data: $('#partner_form').serialize()
    success:(msg) ->
      if msg == 'false'
       console.log(msg)
       $('#captcha').css('border','1px solid #cd6852')
       $('.error_captcha').html('<p style="color: #cd6852; font-weight: bold; text-align: center">Не верный код</p>')
      else
       $('.error_captcha').html()
       $.fancybox.close()