$(window).load ->
  $.page_element = []
  $.screen_height = $(window).height()
  main_page_conteiner = $('#main_page')
  $.main_page_container_height = main_page_conteiner.outerHeight()
  main_page_conteiner.children().each (index, el) ->
    $.page_element[index] = $(el).outerHeight()

  last_news = $('.last_news').offset()
  $.sum_conteiners_before_last_news = last_news.top if last_news
  popular = $('.populars').offset()
  $.sum_conteiners_before_popular = popular.top if popular
  section_0 = $('#section_0').offset()
  $.section_0_offset = section_0.top if section_0
  section_1 = $('#section_1').offset()
  $.section_1_offset = section_1.top if section_1
  section_2 = $('#section_2').offset()
  $.section_2_offset = section_2.top if section_2
  section_3 = $('#section_3').offset()
  $.section_3_offset = section_3.top if section_3

$(document).on 'ready page:load', ->
 $('.popup').popup
  inline: true
  inverted: true
 $('.browse.item.production').popup
   position : 'bottom left'
   hoverable: true
   target: false
   addTouchEvents: false
   exclusive: true
   hideOnScroll: true
   delay:
     show: 200
     hide: 200
 $('.menu.directions .item').tab()
 $('.direction-pagination .page').tab()

 $("#order_to_partner_button").fancybox
   parent: 'body'
   prevEffect: 'elastic'
   nextEffect: 'elastic'
   helpers:
    type: 'inside'

 $("#sign_in_link").fancybox
   parent: 'body'
   prevEffect: 'elastic'
   nextEffect: 'elastic'
   helpers:
    type: 'inside'



 $.datetimepicker.setLocale('ru')
 $("#user_info_phone").mask("+7 (999) 999 99 99",{placeholder:"+7 (___) ___ __ __"})
 $("#user_info_seriya").mask("99 99",{placeholder:"__ __"})
 $("#user_info_number").mask("999999",{placeholder:"______"})
 $("#user_info_date").mask("99.99.9999",{placeholder:"дд.мм.гггг"})
 $('#user_info_date').datetimepicker
   formatDate:'d.m.Y',
   startDate: '01.01.1960'
   dayOfWeekStart: 1
   lang: 'ru'
   timepicker: false
   format: 'd.m.Y'
 body = $('body')
 if body.hasClass('main')
   $(".slide_brand").fancybox
     parent: 'body'
     prevEffect: 'elastic'
     nextEffect: 'elastic'
     width: 700
     maxWidth: 700
     padding: 50, 50, 50, 50
     helpers:
       type: 'inside'


   $.animation_last_news = false
   $.animation_popular = false
   $.animation_catalog_section_0 = false
   $.animation_catalog_section_1 = false
   $.animation_catalog_section_2 = false
   $.animation_catalog_section_3 = false

   section_0 = $('#section_0')
   section_1 = $('#section_1')
   section_2 = $('#section_2')
   section_3 = $('#section_3')
   section_0_img = $('#section_0 img')
   section_1_img = $('#section_1 img')
   section_2_img = $('#section_2 img')
   section_3_img = $('#section_3 img')

   section_0.mouseenter ->
     section_0_img.transition('pulse')
   section_1.mouseenter ->
     section_1_img.transition('pulse')
   section_2.mouseenter ->
     section_2_img.transition('pulse')
   section_3.mouseenter ->
     section_3_img.transition('pulse')

   news = $('.last_news .cell img')
   news.mouseenter ->
     $(@)
       .transition('horizontal flip')
       .transition('horizontal flip')


#   popular = $('.ui.four.cards')
#   popular.hide()
   $('.ui.rating').rating('disable')
   $('.bxslider').bxSlider
     mode: 'horizontal'
     captions: false
     easing: 'easeInOutQuad'
     controls: false
     startSlide: 0
     infiniteLoop: true
     pager: false
     auto: true
     autoControls: false
     pause: 4000
     speed: 500
     useCSS: false
     adaptiveHeight: false
     responsive: false
   $('.brands').bxSlider
     moveSlides: 1
     slideWidth: 150
     minSlides: 1
     maxSlides: 8
     slideMargin: 10
     pager: false
     nextText: '>'
     prevText: '<'
     #controls: false
     #adaptiveHeight: false
     #responsive: false

 if body.hasClass('admin_body')
  $(document).on 'page:change', ->
   if ($('textarea').length > 0)
     data = $('textarea')
     $.each data, (i) ->
       CKEDITOR.replace(data[i].id)
$(document).scroll ->
  body = $('body')
  if body.hasClass('main')
   window_offset = 0
   height_from_top = $('body').scrollTop()
   last_news = $.sum_conteiners_before_last_news - $.screen_height  + 200
   popular = $.sum_conteiners_before_popular - $.screen_height + 200
   section_0 = $.section_0_offset - $.screen_height + 150
   section_1 = $.section_1_offset - $.screen_height + 150
   section_2 = $.section_2_offset - $.screen_height + 150
   section_3 = $.section_3_offset - $.screen_height + 150

   #console.log popular, height_from_top

   if (height_from_top > last_news && !$.animation_last_news)
     $.animation_last_news = true
     img = $('.last_news .cell img')
     img
      .transition('horizontal flip')
      .transition('horizontal flip')
      .transition('horizontal flip')
      .transition('horizontal flip')

   if (height_from_top > section_0   && !$.animation_catalog_section_0)
      $.animation_catalog_section_0 = true
      section_0 = $('#section_0')
      section_0.transition('jiggle')

      #section_0.animate({left: '0'}, 1000)

   if (height_from_top > section_1   && !$.animation_catalog_section_1)
      $.animation_catalog_section_1 = true
      section_1 = $('#section_1')
      section_1.transition('jiggle')
      #section_1.animate({right: '0'}, 1000)

   if (height_from_top > section_2   && !$.animation_catalog_section_2)
      $.animation_catalog_section_2 = true
      section_2 = $('#section_2')
      section_2.transition('jiggle')
      #section_2.animate({left: '0'}, 1000)

   if (height_from_top > section_3   && !$.animation_catalog_section_3)
      $.animation_catalog_section_3 = true
      section_3 = $('#section_3')
      section_3.transition('jiggle')
      #section_3.animate({right: '3'}, 1000)

   if (height_from_top > popular   && !$.animation_popular)
      $.animation_popular = true
      popular = $('.ui.four.cards')
      popular.transition('bounce')

$(document).on 'change', '#aggree', ->
  if ($(@).is(':checked'))
   $('.ui.button').removeClass('disabled')
  else
   $('.ui.button').addClass('disabled')