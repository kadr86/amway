$(document).on 'ready page:load', ->
  $.body = $('body')
  if $.body.hasClass 'catalog_detail'
   $('.ui.rating').rating('disable')
   $(".image_popup").fancybox
      parent: 'body'
      prevEffect: 'elastic'
      nextEffect: 'elastic'
      autoSize: true
      fitToView: true
      helpers:
        type: 'inside'
  if $.body.hasClass 'catalog_index'
    $('.ui.accordion').accordion()
  if $.body.hasClass('catalog_edit_new')
    $('.menu .item').tab()
    $('#catalog_discount').popup()
  if $.body.hasClass('catalog_section')
    $('.brands_catalog .menu .dropdown').dropdown
      on: 'hover'

$(document).on 'click', '.ui.accordion .title', ->
  if $.body.hasClass 'catalog_index'
    $this = $(@)
    menu = $('.ui.accordion').children().find('i')

    menu.removeClass('open')
    if $this.hasClass('active')
      i = $this.children().find('i')
      i.addClass('open')

$(document).on 'click', '.to_cart', (e) ->
  e.preventDefault()

  $this = $(@)
  if $.body.hasClass 'catalog_section'
    id = $(@).attr('data-catalod')
    session = $(@).attr('data-session')
    order = {}
    order['catalog_id'] =  id
    order['order_type'] =  2
    order['session'] =  session
    cart_count = $('.cart_count')
    #Для анимации
    #button = $this.find('.ui.mini.button')
    #button_xy = button.offset()
    #button_x = button_xy.left
    #button_y = button_xy.top
    #cart_xy = cart_count.offset()
    #cart_x = cart_xy.left
    #cart_y = cart_xy.top
    #button.css('position','absolute')

    $.ajax
     url: '/orders/'
     type: 'POST'
     data: {order: order}
     success: (res)->
      cart_count.html(parseInt(cart_count.html()) + 1)
      button = $this.find('.ui.mini.button')
      button.addClass('disabled')
      button.find('.cart_text').html('Добавлен')
  if $.body.hasClass 'catalog_detail'
   id = $(@).attr('data-catalod')
   session = $(@).attr('data-session')
   order = {}
   order['catalog_id'] = id
   order['order_type'] = 2
   order['session'] = session
   cart_count = $('.cart_count')
   $.ajax
    url: '/orders/'
    type: 'POST'
    data: {order: order}
    success: (res)->
     cart_count.html(parseInt(cart_count.html()) + 1)
     $('.button').addClass('disabled').html('<i class="shop icon"></i> Добавлен')