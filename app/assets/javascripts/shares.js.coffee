$(document).on 'ready page:load', ->
  $("#share_date_from").mask("99.99.9999",{placeholder:"дд.мм.гггг"})
  $("#share_date_to").mask("99.99.9999",{placeholder:"дд.мм.гггг"})
  $('#share_date_from').datetimepicker
    formatDate: 'd.m.Y'
    dayOfWeekStart: 1
    lang: 'ru'
    timepicker: false
    format: 'd.m.Y'
  $('#share_date_to').datetimepicker
    formatDate: 'd.m.Y'
    dayOfWeekStart: 1
    lang: 'ru'
    timepicker: false
    format: 'd.m.Y'