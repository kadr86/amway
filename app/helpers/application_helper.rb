module ApplicationHelper
  def getSectionColor color
    colors = {'red' => '#db2828', 'blue' => '#2185d0', 'green' => '#21ba45', 'teal' => 'teal'}
    colors[color]
  end
end
