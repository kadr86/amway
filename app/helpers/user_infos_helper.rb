module UserInfosHelper
  def getUserActiveText active, order_type
    text = ''
    color = ''
    if active == true && order_type == 1
      text = 'Партнер'
      color = '#A3C293'
    end
    if active == false && order_type == 1
      text = 'Не партнер'
      color = '#FFF8DB'
    end
    if active == true && order_type == 2
      text = 'Закрыта'
      color = '#A3C293'
    end
    if active == false && order_type == 2
      text = 'Открыта'
      color = '#FFF8DB'
    end
    {text: text, color: color}
  end
end
