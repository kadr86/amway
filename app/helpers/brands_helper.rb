module BrandsHelper
  def getProducts
    products = Product.all
    products_arr = {}
    unless products.nil?
      products.each {|product| products_arr[product.name] = product.id}
    end
    products_arr
  end

  def getBrandSection section_id
    Brand.where(product_id: section_id)
  end
end
