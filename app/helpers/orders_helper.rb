module OrdersHelper
  def getOrderType
    {1 => 'Стать партнером', 2 => 'Заказ' }
  end



  def getOrderCountsForUser session
    Order.where(session: session, order_type: 2, active: 0).count(:session) || 0
  end

  def getOrderCountsForUserByProduct session, id
    Order.where(session: session, order_type: 2, catalog_id: id, active: 0).count(:session)
  end
end
