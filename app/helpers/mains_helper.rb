module MainsHelper
  def getShares
    Share.show_main
  end

  def getNewsToMain
    SiteNew.limit 4
  end

  def getCatalog
    Product.all
  end

  def getMostPopularProduct
    ratios = Ratio.select(:id, :catalog_id).order('sum(ratio) DESC').group(:catalog_id).limit 4
    ids = []
    unless ratios.nil?
      ratios.each {|ratio| ids << ratio.catalog_id }
    end
    products = []
    catalogs = Catalog.where(id: ids)
    unless catalogs.nil?
      catalogs.each {|catalog| products << {id: catalog.id, name: catalog.name, color: catalog.product.color, picture: catalog.picture.url(:site), ratio: (catalog.ratios.sum(:ratio) / catalog.ratios.length)} }
    end
    products
  end

  def getBrandsMain
    Brand.show_main
  end
end
