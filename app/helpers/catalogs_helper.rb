module CatalogsHelper
  def getSections
    sections = {}
    Product.all.each {|section| sections[section.name] = section.id}
    sections
  end

  def getBrands product_id
    brands = Brand.where product_id: product_id
    brand_arr = {}
    unless brands.nil?
      brands.each {|brand| brand_arr[brand.name] = brand.id}
    end
    brand_arr
  end

  def getBrandsForCatalog product_id
    Brand.where product_id: product_id
  end

  def getRatio id
    sum = Ratio.where(catalog_id: id).sum(:ratio)
    count = Ratio.where(catalog_id: id).count(:ratio)
    unless sum.zero? && count.zero?
      ratio =  (sum/count).round
    else
      ratio = '0'
    end
    if Ratio.where(catalog_id: id).exists?(ip: request.remote_ip) || Ratio.where(catalog_id: id).exists?(session: request.session.id)
      disable = true
    else
      disable = false
    end
    ratio_text = ''
    case ratio
      when 1 then ratio_text = 'Ужасно'
      when 2 then ratio_text = 'Плохо'
      when 3 then ratio_text = 'Нормально'
      when 4 then ratio_text = 'Хорошо'
      when 5 then ratio_text = 'Отлично'
    end
    {ratio: ratio, disable: disable, count: count, ratio_text: ratio_text}
  end
  def getRatioText ratio
    ratio_text = ''
    case ratio
      when 1 then ratio_text = 'Ужасно'
      when 2 then ratio_text = 'Плохо'
      when 3 then ratio_text = 'Нормально'
      when 4 then ratio_text = 'Хорошо'
      when 5 then ratio_text = 'Отлично'
    end
    ratio_text
  end

  def getSubsections section
    subsections = Subsection.where(product_id: section)
    subsections_arr = {}
    unless subsections.nil?
      subsections.each {|subsection| subsections_arr[subsection.name] = subsection.id}
    end
    subsections_arr
  end

  def getSubsectionsForFilter section
    Subsection.where product_id: section
  end
end

