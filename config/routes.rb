Amway::Application.routes.draw do




  root 'mains#index'
  devise_for :users
  mount Ckeditor::Engine => '/ckeditor'

  resources :payment_deliveries do
    get :admin_index, as: :admin, on: :collection
  end
  resources :subsections
  resources :brands do
    get :admin_index, as: :admin, on: :collection
  end

  resources :user_infos do
    get :admin_index, as: :admin, on: :collection
    patch :update_from_site, on: :member
    post :add_request, on: :collection
  end

  resources :orders do
    get :admin_index, as: :admin, on: :collection
    post :accept_order, as: :accept, on: :collection
    get :success, on: :collection
  end
  resources :comments do
    get :admin_index, as: :admin, on: :collection
  end
  resources :products
  resources :photogallereis
  resources :shares do
    get :admin_index, as: :admin, on: :collection
  end
  resources :site_news do
    get :admin_index, as: :admin, on: :collection
  end
  resources :bisneses do
    get :admin_index, as: :admin, on: :collection
  end
  resources :contacts do
    get :admin_index, as: :admin, on: :collection
  end
  resources :mains do
    get :admin_index, as: :admin, on: :collection
  end
  resources :catalogs do
    get :admin_index, as: :admin, on: :collection
    post :add_comment, on: :collection
    get :discount, on: :collection
  end
  get 'catalogs/:code/section' => 'catalogs#section', as: :section_catalog
  get 'catalogs/:code/:brand_id/brand' => 'catalogs#brand', as: :brand_catalogs
  get 'catalogs/:code/:sub_id/section' => 'catalogs#subsection', as: :catalog_subsection
  get 'admin' => 'user_infos#admin_index'
  get 'sitemap' => 'application#sitemap'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
