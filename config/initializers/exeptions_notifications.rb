if Rails.env == 'production'
  exeptions = []
  exeptions << 'ActiveRecord::RecordNotFound'
  exeptions << 'AbstractController::ActionNotFound'
  exeptions << 'ActionController::RoutingError'
  exeptions << 'ActionController::InvalidAuthenticityToken'


  Rails.application.config.middleware.use ExceptionNotification::Rack,
     ignore_exceptions: exeptions,
     email: {
         email_prefix: '[Error 500 Amway]',
         sender_address: 'amway73@site.ru',
         exception_recipients: %w{kadr1986@gmail.com},
     }
end
